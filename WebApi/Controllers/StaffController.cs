﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class StaffController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Staff
        //public IQueryable<staff> Getstaff()
        public IQueryable<staff> Getstaff()
        {
            return db.staff;
        }

        // GET: api/Staff/5
        [ResponseType(typeof(staff))]
        public IHttpActionResult Getstaff(int id)
        {
            staff staff = db.staff.Find(id);
            if (staff == null)
            {
                return NotFound();
            }

            return Ok(staff);
        }

        // PUT: api/Staff/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putstaff(int id, staff staff)
        {
           
            if (id != staff.id)
            {
                return BadRequest();
            }

            db.Entry(staff).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!staffExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Staff
        [ResponseType(typeof(staff))]
        public IHttpActionResult Poststaff(staff staff)
        {
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

            db.staff.Add(staff);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = staff.id }, staff);
        }

        // DELETE: api/Staff/5
        [ResponseType(typeof(staff))]
        public IHttpActionResult Deletestaff(int id)
        {
            staff staff = db.staff.Find(id);
            if (staff == null)
            {
                return NotFound();
            }

            db.staff.Remove(staff);
            db.SaveChanges();

            return Ok(staff);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool staffExists(int id)
        {
            return db.staff.Count(e => e.id == id) > 0;
        }
    }
}