USE [StaffAndClient]
GO
/****** Object:  Table [dbo].[client]    Script Date: 14.05.2021 12:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[client](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[card_number] [int] NOT NULL,
	[name] [nvarchar](30) NOT NULL,
	[surname] [nvarchar](30) NOT NULL,
	[patronymic] [nvarchar](30) NOT NULL,
	[birthday] [date] NOT NULL,
	[register_date] [date] NOT NULL,
	[sale_id] [int] NOT NULL,
 CONSTRAINT [PK_client] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[otdel]    Script Date: 14.05.2021 12:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[otdel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_otdel] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[post]    Script Date: 14.05.2021 12:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[post](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_post] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sale]    Script Date: 14.05.2021 12:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sale](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sale_size] [int] NOT NULL,
 CONSTRAINT [PK_sale] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[staff]    Script Date: 14.05.2021 12:33:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[staff](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NOT NULL,
	[surname] [nvarchar](30) NOT NULL,
	[patronymic] [nvarchar](30) NOT NULL,
	[passport_series] [int] NOT NULL,
	[passport_number] [int] NOT NULL,
	[phone] [nvarchar](30) NOT NULL,
	[address] [nvarchar](60) NOT NULL,
	[bitthday] [date] NOT NULL,
	[post_id] [int] NOT NULL,
	[otdel_id] [int] NOT NULL,
	[login] [nvarchar](50) NULL,
	[password] [nvarchar](200) NULL,
 CONSTRAINT [PK_staff] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[client] ON 

INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (1, 653985645, N'�������� ', N'������� ', N'���������', CAST(N'1993-10-01' AS Date), CAST(N'2009-09-30' AS Date), 1)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (2, 589985645, N'���������� ', N'��������� ', N'���������', CAST(N'2007-05-08' AS Date), CAST(N'2011-12-22' AS Date), 0)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (3, 583985645, N' ������ ', N'��������', N'Ը�������', CAST(N'2012-05-29' AS Date), CAST(N'2012-06-17' AS Date), 3)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (4, 485985645, N'������ ', N'��������� ', N'����������', CAST(N'2013-05-24' AS Date), CAST(N'2012-12-16' AS Date), 4)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (5, 289985645, N'�������', N'�������� ', N'�������� ', CAST(N'2017-10-04' AS Date), CAST(N'2013-04-22' AS Date), 5)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (6, 426985645, N' ���� ', N'��������', N'����������', CAST(N'1983-04-26' AS Date), CAST(N'2015-05-03' AS Date), 2)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (7, 853985645, N' ���� ', N'�������', N'�������������', CAST(N'1988-06-06' AS Date), CAST(N'2016-05-21' AS Date), 3)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (8, 759985645, N'��� ', N'�������� ', N'�������������', CAST(N'1996-02-29' AS Date), CAST(N'2017-12-01' AS Date), 1)
INSERT [dbo].[client] ([id], [card_number], [name], [surname], [patronymic], [birthday], [register_date], [sale_id]) VALUES (9, 753985645, N'������� ', N'������� ', N'��������', CAST(N'1996-03-18' AS Date), CAST(N'2018-12-23' AS Date), 4)
SET IDENTITY_INSERT [dbo].[client] OFF
GO
SET IDENTITY_INSERT [dbo].[otdel] ON 

INSERT [dbo].[otdel] ([id], [title]) VALUES (1, N'��������� �����')
INSERT [dbo].[otdel] ([id], [title]) VALUES (2, N'�������� ����')
INSERT [dbo].[otdel] ([id], [title]) VALUES (3, N'�������� �������')
INSERT [dbo].[otdel] ([id], [title]) VALUES (4, N'����������� ��������')
SET IDENTITY_INSERT [dbo].[otdel] OFF
GO
SET IDENTITY_INSERT [dbo].[post] ON 

INSERT [dbo].[post] ([id], [title]) VALUES (1, N'���-�����.')
INSERT [dbo].[post] ([id], [title]) VALUES (2, N'�����')
INSERT [dbo].[post] ([id], [title]) VALUES (3, N'������� � �������')
INSERT [dbo].[post] ([id], [title]) VALUES (4, N'����������� ��������')
INSERT [dbo].[post] ([id], [title]) VALUES (5, N'������')
INSERT [dbo].[post] ([id], [title]) VALUES (6, N'��������������')
INSERT [dbo].[post] ([id], [title]) VALUES (7, N'��������')
SET IDENTITY_INSERT [dbo].[post] OFF
GO
SET IDENTITY_INSERT [dbo].[sale] ON 

INSERT [dbo].[sale] ([id], [sale_size]) VALUES (0, 0)
INSERT [dbo].[sale] ([id], [sale_size]) VALUES (1, 3)
INSERT [dbo].[sale] ([id], [sale_size]) VALUES (2, 5)
INSERT [dbo].[sale] ([id], [sale_size]) VALUES (3, 7)
INSERT [dbo].[sale] ([id], [sale_size]) VALUES (4, 10)
INSERT [dbo].[sale] ([id], [sale_size]) VALUES (5, 15)
SET IDENTITY_INSERT [dbo].[sale] OFF
GO
SET IDENTITY_INSERT [dbo].[staff] ON 

INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (1, N'��� ', N'������� ', N'��������', 930496, 4299, N'8(073)359-99-83', N'������, �. ������, ������ ��., �. 22 ��.73', CAST(N'1997-10-28' AS Date), 1, 4, N'Eva1', N'123')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (2, N'���� ', N'������ ', N'��������', 458431, 4067, N'8(073)692-27-22', N'������, �. ��������, ����� ������ ��., �. 19 ��.18', CAST(N'1991-06-01' AS Date), 2, 3, N'Artem1', N'1234')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (3, N'�������� ', N'��������� ', N'����������', 430043, 4957, N'8(073)753-13-80', N'������, �. �����, 3 ����� ��., �. 14 ��.146', CAST(N'2001-09-13' AS Date), 3, 2, N'Vik1', N'12345')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (4, N'���� x', N'������ ', N'������ ', 155006, 4344, N'8(073)014-93-64', N'������, �. �����, ���������� ��., �. 2 ��.193', CAST(N'2002-09-12' AS Date), 4, 1, N'Mark1', N'123456')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (5, N'����� ', N'������ ', N'��������', 682118, 4227, N'8(073)834-12-83', N'������, �. ��������, ��������� ��., �. 12 ��.57', CAST(N'1997-08-07' AS Date), 5, 3, N'Agata1', N'1234567')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (6, N'����� ', N'��������� ', N'����������', 703471, 4417, N'8(073)851-68-39', N'������, �. ������, �������� ��., �. 11 ��.202', CAST(N'2002-03-06' AS Date), 6, 2, N'Alica1', N'12345678')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (7, N'�����', N'���������', N' ��������', 772685, 4457, N'8(073)937-31-73', N'������, �. ������������, �������� ��., �. 6 ��.6', CAST(N'1996-09-20' AS Date), 7, 4, N'Daria1', N'123321')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (8, N'����� ', N'������ ', N'����������', 961515, 4989, N'8(073)972-23-63', N'������, �. �����������, ���������� ��., �. 12 ��.204', CAST(N'1996-11-04' AS Date), 3, 1, N'Timyr1', N'1234321')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (9, N'������� ', N'������� ', N'����������', 173404, 4496, N'8(073)974-26-71', N'������, �. ��������, ���������������� ��., �. 15 ��.30', CAST(N'1992-09-04' AS Date), 5, 2, N'Nar1', N'123454321')
INSERT [dbo].[staff] ([id], [name], [surname], [patronymic], [passport_series], [passport_number], [phone], [address], [bitthday], [post_id], [otdel_id], [login], [password]) VALUES (10, N'������ ', N'�������� ', N'����������', 371272, 4917, N'8(073)220-06-06', N'������, �. ����, ���������� ��., �. 18 ��.113', CAST(N'1989-03-18' AS Date), 6, 3, N'Mix1', N'12345654321')
SET IDENTITY_INSERT [dbo].[staff] OFF
GO
ALTER TABLE [dbo].[client]  WITH CHECK ADD  CONSTRAINT [FK_client_sale] FOREIGN KEY([sale_id])
REFERENCES [dbo].[sale] ([id])
GO
ALTER TABLE [dbo].[client] CHECK CONSTRAINT [FK_client_sale]
GO
ALTER TABLE [dbo].[staff]  WITH CHECK ADD  CONSTRAINT [FK_staff_otdel] FOREIGN KEY([otdel_id])
REFERENCES [dbo].[otdel] ([id])
GO
ALTER TABLE [dbo].[staff] CHECK CONSTRAINT [FK_staff_otdel]
GO
ALTER TABLE [dbo].[staff]  WITH CHECK ADD  CONSTRAINT [FK_staff_post] FOREIGN KEY([post_id])
REFERENCES [dbo].[post] ([id])
GO
ALTER TABLE [dbo].[staff] CHECK CONSTRAINT [FK_staff_post]
GO
