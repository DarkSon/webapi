﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace Mvc
{
    public static class GlobalVariables
    {
        public static HttpClient WebApiClient = new HttpClient();


        static GlobalVariables()
        {
            ServicePointManager.ServerCertificateValidationCallback =
              delegate (object s, X509Certificate certificate,
                       X509Chain chain, SslPolicyErrors sslPolicyErrors)
              { return true; };
            WebApiClient.BaseAddress = new Uri("https://localhost:44325/api/");
            WebApiClient.DefaultRequestHeaders.Clear();
            WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}