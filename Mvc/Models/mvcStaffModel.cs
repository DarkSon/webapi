﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mvc.Models
{
    
    public class mvcStaffModel
    {
        
        public int id { get; set; }

        [Required(ErrorMessage = "Error")]
        public string name { get; set; }
        public string surname { get; set; }
        public string patronymic { get; set; }
        public int passport_series { get; set; }
        public int passport_number { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public System.DateTime bitthday { get; set; }
        public int post_id { get; set; }
        public int otdel_id { get; set; }
        public string login { get; set; }
        public string password { get; set; }

        public virtual mvcOtdelModel otdel { get; set; }
        public virtual mvcPostModel post { get; set; }

    }
}