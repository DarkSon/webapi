﻿using Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Mvc;

namespace Mvc.Controllers
{
    public class StaffController : Controller
    {
        // GET: Staff
        public ActionResult Index()
        {
            IEnumerable<mvcStaffModel> staffList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Staff").Result;
            staffList = response.Content.ReadAsAsync<IEnumerable<mvcStaffModel>>().Result;
            return View(staffList);
        }

        public ActionResult AddorEdit(int id = 0)
        {
            return View(new mvcStaffModel());
        }

        [HttpPost]
        public ActionResult AddorEdit(mvcStaffModel staff)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Staff", staff).Result;
            return RedirectToAction("Index");
        }
    }
}